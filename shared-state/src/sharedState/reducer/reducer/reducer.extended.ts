// This file has been generated by reducer-gen (@Stefan Pauka) written in 2020 / 2021.

import { IState } from "./../state";
import { getSharedStateDefaultState } from "./../defaultState.base.generated";
import { SharedStateReducerActions } from "./../reducerActions/reducerActions.main.generated";


// import { SHARED_STATE_ACTIONS_BASE as actions } from "./../actions/actions.base.generated";
// import { SHARED_STATE_ACTIONS_EXTENDED as actions } from "./../actions/actions.extended";

// Uncomment for some typechecking:
// import { isSharedStateReducerActionsExtended } from "./../reducerActions/reducerActions.extended";
// import { isSharedStateReducerActionsBase } from "./../reducerActions/reducerActions.base.generated";

export const sharedStateReducerExtended = (state: IState = getSharedStateDefaultState(), action: SharedStateReducerActions): IState => {
    switch (action.type) {
        //         case actions["[actionName]"]:
        //             return {
        //                 ...state,
        //                 // [action payload]
        //            };
        //         case extendedActions["[actionName]"]:
        //             return {
        //                 ...state,
        //                 // [action payload]
        //              };
        default:
            return state;
    }
};

export default sharedStateReducerExtended;
