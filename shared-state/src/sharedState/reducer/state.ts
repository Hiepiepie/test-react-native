export interface IState {
    // initialized
    webappInitialized: boolean;
    message: string;
    count: number;
    testValue: string;
}
