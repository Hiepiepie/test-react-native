// This file has been generated by reducer-gen (@Stefan Pauka) written in 2020 / 2021.
// DO NOT Change anything inside this file. Every time the generator is used, it will be overwritten.

import { sharedStateActionCreatorsBase } from "./actionCreators.base.generated";
import { sharedStateActionCreatorsExtended } from "./actionCreators.extended";

export const sharedStateActionCreators = { ...sharedStateActionCreatorsBase, ...sharedStateActionCreatorsExtended };

export default sharedStateActionCreators;
