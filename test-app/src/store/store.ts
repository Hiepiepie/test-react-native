import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {sharedStateReducer} from "@matre/sharedstate";
// import {createStore} from "vuex";
import {logger, postMessageOnEveryChangedPropertyMiddleware} from "@/store/utils";

export const reduxstore = configureStore({
    reducer: combineReducers({
        app: sharedStateReducer
    }),
    middleware: [logger, postMessageOnEveryChangedPropertyMiddleware]
})

export const getters = {
    methods: {
        getCount: function () {
            return reduxstore.getState().app.count;
        },
        getMessage: function () {
            return reduxstore.getState().app.message;
            // vuex syntax, but next line should be in computed when using vuex
            // return vuestore.state.message;
        },
        getValue(){
            return reduxstore.getState().app.testValue;
        }
    }
};

// export const vuestore = createStore({
//     state: {
//         count: 0
//     },
//     getters: {
//         count(state) {
//             return state.count
//         }
//     },
//     mutations: {
//         increment(state) {
//             state.count++
//         },
//         setCount(state, count) {
//             state.count = count
//         }
//     },
//     actions: {},
//     modules: {}
// })
