import {
    asSyncStateAction,
    createSyncStateAction,
    SYNC_STATE_ACTION_SOURCE_FRAME,
    SYNC_STATE_ACTION_SOURCE_WEBAPP,
} from "@derfrodo/call-of-action";
import type {ReactNativeWebviewWindow} from "@derfrodo/call-of-action";
import {reduxstore} from "@/store/store";
import {sharedStateActionCreators} from "@matre/sharedstate";


// TODO add logic from usePostMessageCallback.ts, check for origin ..., not exactly as in call of action library, but important ifs should be correct
const receivedMessageHandler = (event: MessageEvent) => {
    const {data, origin, source} = event;
    // a way to see logs from vue in react native
    // reduxstore.dispatch(sharedStateActionCreators.setMessage(origin ))
    if (origin !== window.location.origin) {
        console.debug("Processing posted event: Origin differs", {
            currentOrigin: window.location.origin,
            eventOrigin: origin,
        });
        return;
    }

    if (true && source !== window) {
        console.debug("Processing posted event: Source differs", {
            eventOrigin: origin,
        });
        return;
    }

    const action = JSON.parse(event.data);
    if (action !== null && action.source === SYNC_STATE_ACTION_SOURCE_FRAME) {
        reduxstore.dispatch(action.payload);
    }

    // const action = asSyncStateAction(JSON.parse(event.data), isActionTypeguard);
    // reduxstore.dispatch(JSON.parse(event.data).payload);
};

// similar to useHybridWebAppConsumeSyncStateActionPostMessages
// RECEIVE postMessage(from native) --> ReduxStore (Vue)
// general way that can dispach generated actions from shared-state
export const registerMessageEventConsumer = () => {
    try {
        window.addEventListener("message", receivedMessageHandler, undefined);
    } catch (err) {
        console.warn("Failed to register message events on window", {
            error: err,
        });
    }
    try {
        // for iOS
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (document as unknown as any).addEventListener(
            "message",
            receivedMessageHandler
        );
    } catch (err) {
        console.warn("Failed to register message events on document (iOS)", {
            error: err,
        });
    }

    return (): void => {
        try {
            window.removeEventListener("message", receivedMessageHandler, undefined);
        } catch (err) {
            console.warn("Failed to unregister message events on window", {
                error: err,
            });
        }
        try {
            // for iOS
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (document as unknown as any).removeEventListener(
                "message",
                receivedMessageHandler
            );
        } catch (err) {
            console.warn("Failed to unregister message events on document (iOS)", {
                error: err,
            });
        }
    };

    // window.addEventListener("message", (message) => {
    //     // for example reduxstore.dispatch({"type": 'SHARED_STATE_SET_COUNT', "next": this.getCount() + 1, "isBubbled": true});
    //     const action = asSyncStateAction(JSON.parse(message.data), isActionTypeguard);
    //     reduxstore.dispatch(JSON.parse(message.data).payload);
    // });
};

// subscription to redux store generated using command "./node_modules/.bin/reducer-gen --loglevel info"
// !!! It is necessary to call rerender (forceUpdate) to see updated redux value from store, this is what react redux does inside library
export const subscribeComponentForChangesFromRedux = (component: any) => {
    reduxstore.subscribe(() => {
        component.$forceUpdate();
    });
};

export const postMessageFromVueToReactNative = (next: any, type: any) => {
    const syncStateAction = createSyncStateAction(
        {
            type: type,
            next: next,
            isBubbled: true,
        },
        SYNC_STATE_ACTION_SOURCE_WEBAPP
    );

    const rnWindow = window as ReactNativeWebviewWindow;
    if (rnWindow.ReactNativeWebView && rnWindow.ReactNativeWebView.postMessage) {
        rnWindow.ReactNativeWebView.postMessage(JSON.stringify(syncStateAction));
    }

    // example of message
    // const rnWindow = window as ReactNativeWebviewWindow;
    // if (
    //     rnWindow.ReactNativeWebView &&
    //     rnWindow.ReactNativeWebView.postMessage
    // ) {
    //   rnWindow.ReactNativeWebView.postMessage(
    //       JSON.stringify({
    //         type: "SyncStateAction",
    //         source: "WebApp",
    //         payload: {
    //           type: "SHARED_STATE_SET_COUNT",
    //           next: next,
    //           isBubbled: true,
    //         },
    //       })
    //   )
    // }
};

// TODO find different way to call SHARED_STATE_SET_${obj.key.toUpperCase()}
export const handleChangedPropertiesFromRedux = (
    currentState: any,
    nextState: any,
    action: any
) => {
    const changedProperties: any = [];

    if (!currentState && nextState) {
        Object.keys(nextState).forEach((key) => {
            changedProperties.push({key: key, next: nextState[key]});
        });
    } else if (currentState && nextState) {
        Object.keys(nextState).forEach((key) => {
            if (nextState[key] !== currentState[key]) {
                changedProperties.push({key: key, next: nextState[key]});
            }
        });
    }

    // Do not resend if sync message is sent from Frame
    if (changedProperties && action?.isBubbled !== true) {
        changedProperties.forEach((obj: any) => {
            postMessageFromVueToReactNative(
                obj.next,
                `SHARED_STATE_SET_${obj.key.toUpperCase()}`
            );
        });
    }
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// Middleware that posts sync message to frame if required
export const postMessageOnEveryChangedPropertyMiddleware = (store) => (next) => (action) => {
    let currentState = store.getState().app;
    next(action);

    const handleChange = () => {
        const nextState = store.getState().app;
        if (nextState !== currentState) {
            console.log(
                "From observe which key was changed ",
                currentState,
                nextState,
                action
            );
            // this way we have access to currentState and nextState
            handleChangedPropertiesFromRedux(currentState, nextState, action);
            currentState = nextState;
        }
    };

    handleChange();
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const logger = (store) => (next) => (action) => {
    console.group(action.type);
    console.info("dispatching", action);
    const result = next(action);
    console.log("next state", store.getState());
    console.groupEnd();
    return result;
};

// Redux Store how to access prev state https://stackoverflow.com/questions/47967374/redux-store-to-subscribe-to-a-particular-event-only-store-subscribe
//
// This Can be used in component to subscribe to change
// observeStore(reduxstore, (storeState: any) => {
//   return storeState.app
// }, handleChangedPropertiesFromRedux)
//
// Logic reimplemented as middleware postMessageOnEveryChangedPropertyMiddleware
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const observeStore = (store, select, onChange) => {
    let currentState: any;

    function handleChange() {
        const nextState = select(store.getState());
        if (nextState !== currentState) {
            console.log(
                "From observe which key was changed ",
                currentState,
                nextState
            );
            // this way we have access to currentState and nextState
            onChange(currentState, nextState);
            currentState = nextState;
        }
    }

    const unsubscribe = store.subscribe(handleChange);
    handleChange();
    return unsubscribe;
};
