import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {MessageBoundWebView} from './src/atom/MessageBoundWebView';

import {
  useSharedStateReducerContext,
  sharedStateActionCreators,
  SharedStateReducerContextProvider,
} from '@matre/sharedstate';

const App = () => {
  return (
    <SharedStateReducerContextProvider>
      <MessageBoundWebView />
      <View style={styles.footer}>
        <AppButton />
      </View>
    </SharedStateReducerContextProvider>
  );
};

const AppButton = () => {
  var {dispatch, state} = useSharedStateReducerContext();
  return (
    <>
      <Button
        title={'Test me'}
        onPress={() => {
          dispatch(sharedStateActionCreators.setMessage('MessageFromFrame'));
        }}
      />
      <Text>{state.message}</Text>
    </>
  );
};

const styles = StyleSheet.create({
  footer: {
    backgroundColor: '#fafafa',
    paddingTop: 4,
    // paddingLeft: 4,
    // paddingRight: 4,
    paddingBottom: 4,
    height: 48,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
});

export default App;
