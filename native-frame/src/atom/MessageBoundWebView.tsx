import React, {useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import {WebView} from 'react-native-webview';
import {
  useDispatchOnReactNativeWebViewOnMessage,
  usePostMessageToWebViewOnDispatch,
} from '@derfrodo/call-of-action';
import {
  useSharedStateReducerContext,
  isSharedStateReducerActions,
} from '@matre/sharedstate';

const NOZOOMJAVASCRIPT =
  "const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta);";
export const MessageBoundWebView = () => {
  const webviewRef = useRef<WebView>();

  const context = useSharedStateReducerContext();
  const sharedStateDispatch = context.dispatch;
  usePostMessageToWebViewOnDispatch(webviewRef, context);
  const onMessage = useDispatchOnReactNativeWebViewOnMessage(
    sharedStateDispatch,
    isSharedStateReducerActions,
  );
  return (
    <View style={styles.webViewContainer}>
      <WebView
        ref={obj => {
          webviewRef.current = obj ?? undefined;
        }}
        source={{
          uri: 'http://192.168.2.12:5173/',
        }}
        originWhitelist={['*']}
        onMessage={(...e) => {
          onMessage(...e);
          console.log('RECEIVED MESSAGE', ...e);
        }}
        onLoadEnd={() => {
          console.log('Loaded');
        }}
        style={styles.innerView}
        // prevent zooming in webview
        injectedJavaScript={NOZOOMJAVASCRIPT}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  webViewContainer: {
    // backgroundColor: 'yellow',
    // paddingTop: 4,
    // paddingBottom: 20,
    // paddingRight: 20,
    // paddingLeft: 20,
    flex: 1,
  },
  innerView: {
    backfaceVisibility: 'visible',
    backgroundColor: 'grey',
  },
});
